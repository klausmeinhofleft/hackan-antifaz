package be.rlab.antifaz.command.user

import be.rlab.antifaz.bot.AccessControl
import be.rlab.antifaz.bot.Command
import be.rlab.antifaz.bot.CommandContext
import be.rlab.antifaz.bot.model.ChatType

class ListChatsCommand(private val accessControl: AccessControl) : Command {

    override val name: String = "chat_list"
    override val scope: List<ChatType> = listOf(ChatType.GROUP, ChatType.PRIVATE)

    override fun handle(
        context: CommandContext,
        text: String
    ) {
        val message = accessControl.listChats().filter { savedChat ->
            savedChat.type == ChatType.GROUP.type
        }.joinToString("\n") { savedChat ->
            "${savedChat.id} ${savedChat.title} ${savedChat.description ?: ""}"
        }

        context.talk(message)
    }
}
