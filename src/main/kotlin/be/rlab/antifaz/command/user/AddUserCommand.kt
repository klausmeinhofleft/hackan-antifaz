package be.rlab.antifaz.command.user

import be.rlab.antifaz.bot.AccessControl
import be.rlab.antifaz.bot.Command
import be.rlab.antifaz.bot.CommandContext
import be.rlab.antifaz.bot.model.ChatType
import be.rlab.antifaz.bot.model.User
import be.rlab.antifaz.config.ApplicationConfig
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class AddUserCommand(
    private val config: ApplicationConfig,
    private val accessControl: AccessControl
) : Command {

    private val logger: Logger = LoggerFactory.getLogger(AddUserCommand::class.java)

    data class AddUserInput(
        val userName: String,
        val roles: List<String>
    )
    override val name: String = "user_add"
    override val scope: List<ChatType> = listOf(ChatType.GROUP)

    override fun handle(
        context: CommandContext,
        text: String
    ) {
        val addUserInput: AddUserInput? = context.parseInput(text, """
            ¿A qué usuaria querés agregar? El formato es el siguiente:

            user_name: cthulhu
            roles:
                - ADMIN
                - USER
        """.trimIndent())

        addUserInput?.let {
            if (validateRoles(context, addUserInput)) {
                resolveUser(
                    context = context,
                    userName = addUserInput.userName
                )?.let { newUser ->
                    accessControl.assign(context.chat, newUser, addUserInput.roles)
                    context.talk("Listo!")
                }
            }
        }.let {
            logger.info("[$context] Invalid input")
        }
    }

    private fun validateRoles(
        context: CommandContext,
        addUserInput: AddUserInput
    ): Boolean {
        val rolesNames: List<String> = config.roles.map { role ->
            role.name
        }
        val validRoles: Boolean = addUserInput.roles.all { roleName ->
            rolesNames.any { role ->
                role == roleName
            }
        }
        if (!validRoles) {
            logger.info("[$context] Invalid roles: ${addUserInput.roles}")
            context.talk("Los roles válidos son: ${rolesNames.joinToString(", ")}")
        }

        return validRoles
    }

    private fun resolveUser(
        context: CommandContext,
        userName: String
    ): User? {


        val user: User? = accessControl.findUser(userName)

        if (user == null) {
            logger.info("[$context] User $userName not found")
            context.talk("No conozco a @$userName :(, decile que me hable!")
        }

        return user
    }
}
