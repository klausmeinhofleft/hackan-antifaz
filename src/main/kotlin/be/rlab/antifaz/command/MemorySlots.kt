package be.rlab.antifaz.command

object MemorySlots {
    const val EMAILS_SLOT: String = "emails"
    const val CALENDAR_SLOT: String = "calendar"
}
