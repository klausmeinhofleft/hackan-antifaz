package be.rlab.antifaz.command.email

import be.rlab.antifaz.bot.Command
import be.rlab.antifaz.bot.CommandContext
import be.rlab.antifaz.bot.model.ChatType
import be.rlab.antifaz.email.model.Flag

class ListEmailCommand(
    private val emailService: EmailService
) : Command {

    override val name: String = "email_list"
    override val scope: List<ChatType> = listOf(ChatType.GROUP, ChatType.PRIVATE)

    override fun handle(
        context: CommandContext,
        text: String
    ) {
        emailService.findEmailReader(context.chat)?.let { emailReader ->
            context.talk("Bancame que estoy leyendo la bandeja de entrada...")

            val messages = emailReader.listMessages(
                emailReader.openFolder("INBOX"),
                Flag.UNSEEN
            )
            if (messages.isNotEmpty()) {
                val summary = (0 until messages.size).fold("") { result, index ->
                    val message = messages[index]
                    result + "[$index] Subject: ${message.subject}\n"
                }

                context.talk(summary)
            } else {
                context.talk("No hay mensajes nuevos")
            }
        } ?: context.talk("No hay una cuenta de email configurada para este chat")
    }
}
