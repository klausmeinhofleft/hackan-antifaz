package be.rlab.antifaz.command.email

import be.rlab.antifaz.bot.AccessControl
import be.rlab.antifaz.bot.Command
import be.rlab.antifaz.bot.CommandContext
import be.rlab.antifaz.bot.model.ChatType
import be.rlab.antifaz.email.model.Flag
import be.rlab.antifaz.email.model.Message
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ReadEmailCommand(
    private val emailService: EmailService,
    private val accessControl: AccessControl
) : Command {

    private val logger: Logger = LoggerFactory.getLogger(ReadEmailCommand::class.java)

    override val name: String = "email_read"
    override val scope: List<ChatType> = listOf(ChatType.GROUP)

    override fun handle(
        context: CommandContext,
        text: String
    ) {
        val messageIndex: Int? = context.parseInput(text, """
            Ingresá el número de mensaje que querés leer. Para ver los mensajes usá /email_list
        """.trimIndent())

        messageIndex?.let {
            if (messageIndex < 0) {
                context.talk("Elegí uno de los números que te indica /email_list")
                return
            }

            emailService.findEmailReader(context.chat)?.let { emailReader ->
                val messages: List<Message> = emailReader.listMessages(
                    emailReader.openFolder("INBOX"),
                    Flag.UNSEEN
                )

                if (messageIndex >= messages.size) {
                    context.talk("Elegí uno de los números que te indica /email_list")
                    return
                }

                val message = messages[messageIndex]
                context.talk(message.body)

                accessControl.getAdminsIds(context.chat).forEach { adminId ->
                    context.talkTo(adminId, """
                        Se leyó un email de ${message.from} en el chat
                        ${context.chat.title}. Subject: ${message.subject}
                    """.trimIndent())
                }
            }
        }.let {
            logger.info("[$context] Invalid input")
        }
    }
}
