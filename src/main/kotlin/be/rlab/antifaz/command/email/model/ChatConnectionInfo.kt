package be.rlab.antifaz.command.email.model

import be.rlab.antifaz.email.model.ConnectionInfo

data class ChatConnectionInfo(
    val chatId: Long,
    val host: String,
    val port: Int,
    val userName: String,
    val password: String,
    val connectionProperties: Map<String, String> = emptyMap()
) {
    fun asConnectionInfo(): ConnectionInfo = ConnectionInfo(
        host = host,
        port = port,
        userName = userName,
        password = password,
        connectionProperties = connectionProperties
    )

    override fun toString(): String = "$host/$userName"
}
