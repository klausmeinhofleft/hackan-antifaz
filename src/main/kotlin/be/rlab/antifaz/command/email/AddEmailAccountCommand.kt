package be.rlab.antifaz.command.email

import be.rlab.antifaz.bot.AccessControl
import be.rlab.antifaz.bot.Command
import be.rlab.antifaz.bot.CommandContext
import be.rlab.antifaz.bot.Memory
import be.rlab.antifaz.bot.model.Chat
import be.rlab.antifaz.bot.model.ChatType
import be.rlab.antifaz.command.MemorySlots.EMAILS_SLOT
import be.rlab.antifaz.command.email.model.ChatConnectionInfo
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class AddEmailAccountCommand(
    memory: Memory,
    private val accessControl: AccessControl
) : Command {

    private val logger: Logger = LoggerFactory.getLogger(AddEmailAccountCommand::class.java)

    override val name: String = "email_add_account"
    override val scope: List<ChatType> = listOf(ChatType.PRIVATE)

    private var connections: List<ChatConnectionInfo> by memory.slot(EMAILS_SLOT, listOf<ChatConnectionInfo>())

    override fun handle(
        context: CommandContext,
        text: String
    ) {
        try {
            val newConnectionInfo: ChatConnectionInfo? = context.parseInput(text, """
                Ahora mandame la información del servidor IMAPS y de autenticación. El formato es el siguiente:

                chat_id: -347160658
                host: mail.rlab.be
                port: 993
                user_name: cthulhu
                password: rlyeh
            """.trimIndent())

            newConnectionInfo?.let {
                resolveChat(
                    context = context,
                    connectionInfo = newConnectionInfo
                )?.let { targetChat ->
                    connections = resolveConnections(newConnectionInfo)

                    context.talkTo(targetChat, "@${context.user.userName} registró una cuenta de correo para este chat.")
                    context.talk("Genial, ya podés usar los comandos de email en el chat.")
                }
            }.let {
                logger.info("[$context] Invalid input")
            }
        } catch (cause: Exception) {
            context.talk("Hubo un error tratando de agregar la cuenta de email: ${cause.localizedMessage}")
            context.talk("Intentá de nuevo más tarde")
        }
    }

    private fun resolveChat(
        context: CommandContext,
        connectionInfo: ChatConnectionInfo
    ): Chat? {

        val targetChat: Chat? = accessControl.findChat(connectionInfo.chatId)

        if (targetChat == null) {
            logger.info("[$context] Chat not found: ${connectionInfo.chatId}")
            context.talk("El chat con id ${connectionInfo.chatId} no existe.")
        }

        return targetChat
    }

    private fun resolveConnections(newConnectionInfo: ChatConnectionInfo): List<ChatConnectionInfo> {
        val exists = connections.any { existingConnectionInfo ->
            existingConnectionInfo.chatId == newConnectionInfo.chatId
        }

        return if (exists) {
            logger.info("Connection exists, removing: $newConnectionInfo")

            val index = connections.indexOfFirst { existingConnectionInfo ->
                existingConnectionInfo.chatId == newConnectionInfo.chatId
            }
            val newConnections = connections.toMutableList()
            newConnections.removeAt(index)
            newConnections + newConnectionInfo
        } else {
            logger.info("Adding new connection: $newConnectionInfo")
            connections + newConnectionInfo
        }
    }
}
