package be.rlab.antifaz.command.email

import be.rlab.antifaz.bot.Antifaz
import be.rlab.antifaz.bot.BotAware
import be.rlab.antifaz.bot.Memory
import be.rlab.antifaz.bot.model.Chat
import be.rlab.antifaz.command.MemorySlots.EMAILS_SLOT
import be.rlab.antifaz.command.email.model.ChatConnectionInfo
import be.rlab.antifaz.email.EmailReader
import be.rlab.antifaz.email.model.Flag
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class EmailService(memory: Memory) : BotAware {

    private val logger: Logger = LoggerFactory.getLogger(EmailService::class.java)

    private val connections: List<ChatConnectionInfo> by memory.slot(EMAILS_SLOT, listOf<ChatConnectionInfo>())
    private val emailReaders: MutableMap<Long, EmailReader> = connections.map { connectionInfo ->
        connectionInfo.chatId to createEmailReader(connectionInfo)
    }.toMap(mutableMapOf())

    override lateinit var antifaz: Antifaz

    init {
        logger.info("Registering listener for slot '$EMAILS_SLOT' changes")
        memory.subscribe(EMAILS_SLOT, this::updateConnections)
    }

    fun findEmailReader(chat: Chat): EmailReader? {
        logger.debug("Finding email reader for chat $chat (exists: ${emailReaders.containsKey(chat.id)})")
        return emailReaders[chat.id]
    }

    private fun updateConnections() {
        logger.info("'$EMAILS_SLOT' changed, analyzing new email accounts")

        // Closes connection and reconnects to existing servers
        val evicted = emailReaders.filter { (chatId, _) ->
            connections.any { connectionInfo ->
                connectionInfo.chatId == chatId
            }
        }.map { (chatId, emailReader) ->
            logger.info("Account ${emailReader.accountDescription} removed from chat $chatId, shutting down email reader")
            emailReader.shutdown()
            chatId
        }
        evicted.forEach { chatId ->
            emailReaders.remove(chatId)
        }

        // Opens new connections
        connections.filter { connectionInfo ->
            !emailReaders.containsKey(connectionInfo.chatId)
        }.forEach { connectionInfo ->
            logger.info("Account $connectionInfo added for chat ${connectionInfo.chatId}")
            emailReaders[connectionInfo.chatId] = createEmailReader(connectionInfo)
        }
    }

    private fun createEmailReader(connectionInfo: ChatConnectionInfo): EmailReader {
        logger.info("Creating email reader for account $connectionInfo on chat ${connectionInfo.chatId}")

        return EmailReader.connect(connectionInfo.asConnectionInfo()) {
            openFolder("INBOX") { folder ->
                onMessage(folder, Flag.UNSEEN) { message ->
                    antifaz.sendMessage(connectionInfo.chatId, "Hay un nuevo mensaje: ${message.subject}")
                }
            }
        }
    }
}