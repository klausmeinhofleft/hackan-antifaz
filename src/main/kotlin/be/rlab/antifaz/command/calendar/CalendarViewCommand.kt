package be.rlab.antifaz.command.calendar

import be.rlab.antifaz.bot.Command
import be.rlab.antifaz.bot.CommandContext
import be.rlab.antifaz.bot.Memory
import be.rlab.antifaz.command.calendar.model.Event
import be.rlab.antifaz.bot.model.ChatType
import be.rlab.antifaz.command.MemorySlots.CALENDAR_SLOT
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*

class CalendarViewCommand(memory: Memory) : Command {

    override val name: String = "calendar"
    override val scope: List<ChatType> = listOf(ChatType.GROUP, ChatType.PRIVATE)
    private val eventsByChat: Map<Long, List<Event>> by memory.slot(CALENDAR_SLOT, mapOf<Long, List<Event>>())

    private val monthFormatter: DateTimeFormatter = DateTimeFormat
        .forPattern("MMMM YYYY")
        .withLocale(Locale("es", "AR"))
    private val dayFormatter: DateTimeFormatter = DateTimeFormat
        .forPattern("EEEE d")
        .withLocale(Locale("es", "AR"))
    private val hourFormatter: DateTimeFormatter = DateTimeFormat
        .forPattern("HH:mm")

    override fun handle(
        context: CommandContext,
        text: String
    ) {
        val calendar = calendar(context.chat.id)
        val calendarString = StringBuilder()

        if (calendar.isEmpty()) {
            context.talk("No hay eventos en el calendario")
        }

        calendar.forEach { (month, events) ->
            calendarString.append("# ${month.toString(monthFormatter)}\n")

            groupByDay(events).forEach { (day, events) ->
                calendarString.append("  ## ${day.toString(dayFormatter)} \n")

                events.forEach { event ->
                    val end = event.end?.let {
                        "-${event.end.toString(hourFormatter)}"
                    } ?: ""

                    calendarString.append("    ${event.start.toString(hourFormatter)}$end -> ")
                    calendarString.append("${event.title}: ${event.description}\n")
                }
            }
        }

        context.talk(calendarString.toString())
    }

    private fun calendar(chatId: Long): Map<DateTime, List<Event>> {
        return eventsByChat[chatId]?.groupBy { event ->
            event.start.withDayOfMonth(1).withTimeAtStartOfDay()
        } ?: emptyMap()
    }

    private fun groupByDay(events: List<Event>): Map<DateTime, List<Event>> {
        return events.filter { event ->
            event.start.isAfter(DateTime.now().plusDays(1))
        }.groupBy { event ->
            event.start.withTimeAtStartOfDay()
        }
    }
}