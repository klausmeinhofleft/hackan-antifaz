package be.rlab.antifaz.command.calendar

import be.rlab.antifaz.bot.Command
import be.rlab.antifaz.bot.CommandContext
import be.rlab.antifaz.bot.Memory
import be.rlab.antifaz.command.calendar.model.Event
import be.rlab.antifaz.bot.model.ChatType
import be.rlab.antifaz.command.MemorySlots.CALENDAR_SLOT
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class CalendarAddEventCommand(memory: Memory) : Command {

    override val name: String = "calendar_add_event"
    override val scope: List<ChatType> = listOf(ChatType.GROUP, ChatType.PRIVATE)
    private var eventsByChat: Map<Long, List<Event>> by memory.slot(CALENDAR_SLOT, mapOf<Long, List<Event>>())

    private val hourFormatter: DateTimeFormatter = DateTimeFormat
        .forPattern("HH:mm")

    data class AddEventInput(
        @JsonFormat(pattern = "YYYY-MM-dd") @JsonProperty("fecha")
        val date: DateTime,
        @JsonProperty("inicio") val start: String,
        @JsonProperty("fin") val end: String?,
        @JsonProperty("titulo") val title: String,
        @JsonProperty("descripcion") val description: String
    )

    override fun handle(
        context: CommandContext,
        text: String
    ) {
        val addEventInput: AddEventInput? = context.parseInput(text, """
            Mandame los datos del evento en el siguiente formato:

            fecha: 2019-04-27
            inicio: 10:00
            fin: 13:00 (opcional)
            titulo: Apertura del FLISoL
            descripcion: primera charla del FLISoL
        """.trimIndent())

        addEventInput?.let {
            val event = Event.new(
                start = addEventInput.date.withTime(
                    hourFormatter.parseDateTime(addEventInput.start).toLocalTime()
                ),
                end = addEventInput.end?.let {
                    addEventInput.date.withTime(
                        hourFormatter.parseDateTime(addEventInput.end).toLocalTime()
                    )
                },
                title = addEventInput.title,
                description = addEventInput.description
            )

            val events: List<Event> = eventsByChat[context.chat.id] ?: emptyList()

            eventsByChat = eventsByChat + (
                context.chat.id to (events + event).sortedBy { existingEvent ->
                    existingEvent.start
                }
            )

            context.talk("Listo, ya agregué el evento")
        }
    }
}
