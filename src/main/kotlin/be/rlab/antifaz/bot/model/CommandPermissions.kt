package be.rlab.antifaz.bot.model

data class CommandPermissions(
    val commandName: String,
    val requiredPermissions: List<Permission>
)
