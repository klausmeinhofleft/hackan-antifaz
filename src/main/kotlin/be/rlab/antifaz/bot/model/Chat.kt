package be.rlab.antifaz.bot.model

data class Chat(
    val id: Long,
    val type: String,
    val title: String?,
    val description: String?
) {
    override fun toString(): String =
        "$id/$title"
}
