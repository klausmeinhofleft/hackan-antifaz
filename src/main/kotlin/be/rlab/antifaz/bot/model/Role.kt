package be.rlab.antifaz.bot.model

data class Role(
    val name: String,
    val permissions: List<Permission>
) {
    fun isAdmin(): Boolean = permissions.any { permission ->
        permission.admin
    }
}