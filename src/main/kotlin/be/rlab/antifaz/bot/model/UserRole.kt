package be.rlab.antifaz.bot.model

import com.fasterxml.jackson.annotation.JsonIgnore

data class UserRole(
    val chatId: Long,
    val userId: Long,
    val rolesNames: List<String>
) {
    companion object {
        fun new(
            userId: Long,
            rolesNames: List<String>
        ): UserRole = UserRole(
            chatId = userId,
            userId = userId,
            rolesNames = rolesNames
        )

        fun new(
            chatId: Long,
            userId: Long,
            rolesNames: List<String>
        ): UserRole = UserRole(
            chatId = chatId,
            userId = userId,
            rolesNames = rolesNames
        )

        fun key(
            chatId: Long,
            userId: Long
        ): String = "${chatId}_$userId"

        fun key(
            userId: Long
        ): String = "${userId}_$userId"
    }

    @JsonIgnore
    val key: String = key(chatId, userId)
}
