package be.rlab.antifaz.bot.model

enum class ChatType(val type: String) {
    PRIVATE("private"),
    GROUP("group"),
    UNKNOWN("unknown");

    companion object {
        fun from(type: String): ChatType {
            return values().find { chatType ->
                chatType.type == type
            } ?: UNKNOWN
        }
    }
}
