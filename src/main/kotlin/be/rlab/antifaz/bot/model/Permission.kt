package be.rlab.antifaz.bot.model

enum class Permission(
    private val includes: List<Permission> = listOf(),
    val admin: Boolean = false
) {
    READ_EMAILS,
    MANAGE_EMAILS(listOf(READ_EMAILS)),
    MANAGE_USERS,
    READ_CHAT,
    READ_CALENDAR,
    MANAGE_CALENDAR(listOf(READ_CALENDAR)),
    MANAGE_CHAT(listOf(READ_CHAT)),
    MANAGE_ADMINS(listOf(MANAGE_EMAILS, MANAGE_USERS, MANAGE_CHAT, MANAGE_CALENDAR), admin = true);

    fun contains(permission: Permission): Boolean {
        return permission == this || includes.any { includedPermission ->
            includedPermission.contains(permission)
        }
    }
}
