package be.rlab.antifaz.bot

import be.rlab.antifaz.util.ObjectMapperFactory
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import kotlin.reflect.KProperty

data class Memory(
    val memoryDir: File,
    val data: MutableMap<String, String>
) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(Memory::class.java)

        fun wakeUp(memoryDir: File): Memory {
            logger.info("Scanning memory directory to load memory slots: $memoryDir")

            return Memory(
                memoryDir = memoryDir,
                data = memoryDir.listFiles { file ->
                    file.isFile && file.name.endsWith(".json")
                }.map { file ->
                    val slotName: String = file.name.substringBeforeLast(".")

                    logger.info("Loading memory slot '$slotName' from $file")

                    slotName to file.bufferedReader().readText()
                }.toMap(mutableMapOf())
            )
        }
    }

    private val subscribers: MutableMap<String, List<() -> Unit>> = mutableMapOf()

    class SlotResolver(
        val memory: Memory,
        val slotName: String,
        val defaultValue: Any
    ) {
        inline operator fun <reified T> getValue(thisRef: Any?, property: KProperty<*>): T {
            return memory.read(slotName) ?: defaultValue as T
        }

        inline operator fun <reified T> setValue(thisRef: Any?, property: KProperty<*>, value: T) {
            memory.save(slotName, value)
        }
    }

    fun <T> slot(
        slotName: String,
        defaultValue: T
    ) = SlotResolver(
        memory = this,
        slotName = slotName,
        defaultValue = defaultValue as Any
    )

    inline fun <reified T> read(slotName: String): T? {
        logger.debug("Reading slot '$slotName'")

        return data[slotName]?.let { jsonData ->
            ObjectMapperFactory.snakeCaseMapper.readValue(jsonData)
        }
    }

    fun<T> save(
        slotName: String,
        element: T
    ): T {
        val memoryFile = File(memoryDir, "$slotName.json")
        logger.debug("Serializing and saving slot '$slotName' to $memoryFile")
        val jsonSlot = ObjectMapperFactory.snakeCaseMapper.writeValueAsString(element)

        data[slotName] = jsonSlot
        memoryFile.writeText(jsonSlot)

        logger.debug("Invoking subscriptions for slot '$slotName'")
        subscribers.getOrDefault(slotName, listOf()).forEach { callback ->
            callback()
        }

        return element
    }

    fun subscribe(
        slotName: String,
        callback: () -> Unit
    ) {
        logger.info("New subscription to slot '$slotName'")

        val callbacks = subscribers.getOrDefault(slotName, listOf())
        subscribers[slotName] = callbacks + callback
    }
}
