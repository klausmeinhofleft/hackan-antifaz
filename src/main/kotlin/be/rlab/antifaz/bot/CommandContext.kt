package be.rlab.antifaz.bot

import be.rlab.antifaz.bot.model.Chat
import be.rlab.antifaz.bot.model.User
import be.rlab.antifaz.util.ObjectMapperFactory
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory

data class CommandContext(
    private val antifaz: Antifaz,
    private val data: MutableMap<String, Any>,
    val command: Command,
    val chat: Chat,
    val user: User,
    var retries: Int
) {
    companion object {
        fun new(
            antifaz: Antifaz,
            command: Command,
            chat: Chat,
            user: User
        ): CommandContext = CommandContext(
            antifaz = antifaz,
            command = command,
            chat = chat,
            user = user,
            retries = 0,
            data = mutableMapOf()
        )
    }

    val logger: Logger = LoggerFactory.getLogger(CommandContext::class.java)

    fun talk(text: String) {
        antifaz.sendMessage(chat, text)
    }

    fun talkTo(targetChat: Chat, text: String) {
        antifaz.sendMessage(targetChat, text)
    }

    fun talkTo(chatId: Long, text: String) {
        antifaz.sendMessage(chatId, text)
    }

    fun done() {
        retries = 0
    }

    fun retry() {
        retries += 1
    }

    fun set(
        key: String,
        value: Any
    ) {
        data[key] = value
    }

    fun<T> get(
        key: String
    ): T = data[key] as T ?: throw RuntimeException("Key not found: $key")

    fun<T> get(
        key: String,
        defaultValue: T
    ): T = data.getOrPut(key) {
        defaultValue as Any
    } as T

    inline fun<reified T> parseInput(text: String, helpMessage: String): T? {
        return try {
            val result: T = ObjectMapperFactory.yamlMapper.readValue(text)
            done()
            result
        } catch (cause: Exception) {
            logger.error("Error parsing input", cause)
            talk(helpMessage)
            retry()
            null
        }
    }

    override fun toString(): String = "$user on $chat"
}
