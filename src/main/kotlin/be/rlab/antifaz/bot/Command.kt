package be.rlab.antifaz.bot

import be.rlab.antifaz.bot.model.ChatType

interface Command {
    val name: String
    val scope: List<ChatType>

    fun handle(
        context: CommandContext,
        text: String
    )
}
