package be.rlab.antifaz.bot

/** Must be implemented in order to inject Antifaz into a component.
 */
interface BotAware {
    var antifaz: Antifaz
}