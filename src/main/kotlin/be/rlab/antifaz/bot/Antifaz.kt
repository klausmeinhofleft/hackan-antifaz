package be.rlab.antifaz.bot

import be.rlab.antifaz.bot.model.Chat
import be.rlab.antifaz.bot.model.ChatType
import be.rlab.antifaz.bot.model.User
import me.ivmg.telegram.bot
import me.ivmg.telegram.dispatch
import me.ivmg.telegram.dispatcher.command
import me.ivmg.telegram.dispatcher.text
import me.ivmg.telegram.entities.Message
import me.ivmg.telegram.entities.ParseMode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Antifaz(
    accessToken: String,
    commands: List<Command>,
    botListeners: List<BotAware>,
    private val accessControl: AccessControl
) {

    companion object {
        private const val MAX_RETRIES: Int = 3
        private const val ECHO: String = "echo"
    }

    private val logger: Logger = LoggerFactory.getLogger(Antifaz::class.java)

    /** Indicates whether Antifaz is taken by a command.
     * When he's taken by a command, all messages are redirected to the command
     * until it is released.
     */
    private val activeCommands: MutableMap<String, CommandContext> = mutableMapOf()

    private val bot = bot {
        logger.info("Creating bot with access token $accessToken")

        token = accessToken

        dispatch {
            commands.forEach { command ->
                logger.info("Registering command handler: /${command.name}")

                command(command.name) { _, update ->
                    val message = update.message
                        ?: throw RuntimeException("Message not found")

                    releaseIfRequired(message)

                    handleCommand(
                        command = command,
                        message = message
                    )
                }
            }
            text { _, update ->
                val message = update.message
                    ?: throw RuntimeException("Message not found")

                accessControl.addUserIfRequired(message)
                resumeCommandIfRequired(message)
            }
        }
    }

    init {
        logger.info("Registering bot into BotAware listeners")
        botListeners.forEach { botListener ->
            botListener.antifaz = this
        }
    }

    fun start() {
        logger.info("Start polling")
        bot.startPolling()
    }

    fun sendMessage(
        chat: Chat,
        text: String
    ) = sendMessage(chat.id, text)

    fun sendMessage(
        chatId: Long,
        text: String
    ) = bot.sendMessage(
        chatId = chatId,
        text = text,
        disableWebPagePreview = true
    )

    private fun handleCommand(
        command: Command,
        message: Message
    ) {
        val chat: Chat = accessControl.addChatIfRequired(message)
        val user = accessControl.addUserIfRequired(message)

        if (checkScope(command, chat) && checkPermissions(command, chat, user)) {
            val commandContext = findCommandContext(chat, user, CommandContext.new(
                antifaz = this,
                chat = chat,
                user = user,
                command = command
            ))

            logger.info("Executing command /${command.name} on chat $chat by user $user")

            command.handle(commandContext, message.text ?: "")

            if (commandContext.retries == 0) {
                logger.info("/${command.name} on chat $chat by user $user finished")
                releaseIfRequired(message)
            } else if (commandContext.retries == 1) {
                logger.info("/${command.name} on chat $chat by user $user failed, retrying")
                commandContext.set(ECHO, true)
            }
        }
    }

    private fun checkScope(
        command: Command,
        chat: Chat
    ): Boolean {
        val validScope = command.scope.contains(ChatType.from(chat.type))

        logger.info("/${command.name} command scope: ${command.scope}")

        if (!validScope) {
            logger.info("Invalid scope for command /${command.name}: ${ChatType.from(chat.type)}")

            when (ChatType.from(chat.type)) {
                ChatType.PRIVATE ->
                    sendMessage(chat, "Este comando no se puede ejecutar en un chat privado, hablame por el grupo")
                ChatType.GROUP ->
                    sendMessage(chat, "Este comando no se puede ejecutar en un chat público, hablame por privado")
                else ->
                    sendMessage(chat, "Este comando no se puede ejecutar en este tipo de chat")
            }
        }

        return validScope
    }

    private fun checkPermissions(
        command: Command,
        chat: Chat,
        user: User
    ): Boolean {
        val validPermissions = accessControl.checkPermissions(command, chat, user)

        if (!validPermissions) {
            logger.info("User $user has no permissions to execute command /${command.name}")
            sendMessage(chat, "No tenés permisos para usarme :(")
        }

        return validPermissions
    }

    private fun findCommandContext(
        chat: Chat,
        user: User,
        defaultValue: CommandContext? = null
    ): CommandContext {
        return activeCommands.getOrPut(commandContextKey(chat, user)) {
            defaultValue
                ?: throw RuntimeException("Default last command not initialized for chat: ${chat.id}")
        }
    }

    private fun releaseIfRequired(message: Message) {
        val chat = accessControl.addChatIfRequired(message)
        val user = accessControl.addUserIfRequired(message)

        activeCommands.remove(commandContextKey(chat, user))
    }

    private fun resumeCommandIfRequired(message: Message) {
        val chat: Chat = accessControl.addChatIfRequired(message)
        val user: User = accessControl.addUserIfRequired(message)

        if (!activeCommands.containsKey(commandContextKey(chat, user))) {
            return
        }

        val commandContext = findCommandContext(chat, user)

        if (commandContext.get(ECHO)) {
            commandContext.set(ECHO, false)
        } else {
            logger.info("Command /${commandContext.command.name} in progress for user $user on chat $chat, resuming")

            handleCommand(
                command = commandContext.command,
                message = message
            )

            if (commandContext.retries > MAX_RETRIES) {
                logger.info("Command /${commandContext.command.name} reached the max allowed retries")

                sendMessage(chat, "Ya está, intentá usar el comando de nuevo")
                releaseIfRequired(message)
            }
        }
    }

    private fun commandContextKey(
        chat: Chat,
        user: User
    ): String =
        "${chat.id}-${user.id}"
}
