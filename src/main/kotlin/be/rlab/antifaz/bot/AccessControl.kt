package be.rlab.antifaz.bot

import be.rlab.antifaz.bot.model.*
import be.rlab.antifaz.config.ApplicationConfig
import me.ivmg.telegram.entities.Message
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class AccessControl(
    private val config: ApplicationConfig,
    memory: Memory
) {

    companion object {
        private const val CHATS_SLOT: String = "chats"
        private const val USERS_SLOT: String = "users"
        private const val USERS_ROLES_SLOT: String = "users_roles"
    }

    private val logger: Logger = LoggerFactory.getLogger(AccessControl::class.java)

    private var userRoles: Map<String, UserRole> by memory.slot(USERS_ROLES_SLOT, mapOf<String, UserRole>())
    private var chats: List<Chat> by memory.slot(CHATS_SLOT, listOf<Chat>())
    private var users: List<User> by memory.slot(USERS_SLOT, listOf<User>())

    fun checkPermissions(
        command: Command,
        chat: Chat,
        user: User
    ): Boolean {
        val userInfo = userInfo(chat, user)
        logger.info("Checking permissions for user $userInfo to execute command /${command.name}")

        return resolveUserRole(chat, user)?.let { userRole ->
            val requiredPermissions: List<Permission> = config.findCommandPermissions(command.name)
            val userPermissions: List<Permission> = resolveRoles(userRole.rolesNames).flatMap { role ->
                role.permissions
            }
            logger.info("Command ${command.name} requires permissions: $requiredPermissions")
            logger.info("User $userInfo has permissions: $userPermissions")

            val isAdmin: Boolean = isAdmin(userRole.rolesNames)

            isAdmin || requiredPermissions.all { requiredPermission ->
                userPermissions.any { userPermission ->
                    !userPermission.contains(requiredPermission)
                }
            }
        }.let {
            logger.info("User $userInfo not found, checking configured admins")
            config.isAdmin(user.id)
        }
    }

    fun getAdminsIds(chat: Chat): List<Long> {
        return userRoles.filter { (_, userRole) ->
            isAdmin(userRole.rolesNames) && userRole.chatId == chat.id
        }.map { (_, userRole) ->
            userRole.userId
        }.distinct()
    }

    fun assign(
        chat: Chat,
        user: User,
        rolesNames: List<String>
    ) {
        val admin: Boolean = isAdmin(rolesNames)
        val userRole = UserRole.new(chat.id, user.id, rolesNames)

        logger.info("Setting roles for user ${userInfo(chat, user)}: $rolesNames")

        userRoles = userRoles + (userRole.key to userRole)

        if (admin) {
            logger.info("Setting up new admin user ${user.id}/${user.userName}: $rolesNames")

            val adminUserRole = UserRole.new(user.id, rolesNames)
            userRoles = userRoles + (adminUserRole.key to adminUserRole)
        }
    }

    fun addChatIfRequired(message: Message): Chat {
        return chats.find { chat ->
            chat.id == message.chat.id
        } ?: addChat(message)
    }

    fun addUserIfRequired(message: Message): User {
        return users.find { user ->
            user.id == message.from?.id
        } ?: addUser(message)
    }

    fun findChat(chatId: Long): Chat? {
        return chats.find { chat ->
            chat.id == chatId
        }
    }

    fun findUser(userName: String): User? {
        return users.find { user ->
            user.userName == userName
        }
    }

    fun listChats(): List<Chat> =
        chats.toList()

    private fun resolveUserRole(
        chat: Chat,
        user: User
    ): UserRole? {
        return when (ChatType.from(chat.type)) {
            ChatType.PRIVATE -> userRoles[UserRole.key(user.id)]
            else -> userRoles[UserRole.key(chat.id, user.id)]
        }
    }

    private fun resolveRoles(rolesNames: List<String>): List<Role> {
        return rolesNames.map { roleName ->
            config.findRole(roleName)
        }
    }

    private fun isAdmin(rolesNames: List<String>): Boolean {
        return resolveRoles(rolesNames).any { role ->
            role.isAdmin()
        }
    }

    private fun addChat(message: Message): Chat {
        val newChat = Chat(
            id = message.chat.id,
            type = message.chat.type,
            title = message.chat.title,
            description = message.chat.description
        )
        logger.info("Adding new chat $newChat")

        chats = chats + newChat

        return newChat
    }

    private fun addUser(message: Message): User {
        return message.from?.let { user ->
            val newUser = User(
                id = user.id,
                userName = user.username,
                firstName = user.firstName,
                lastName = user.lastName
            )

            logger.info("Adding new user $newUser")

            users = users + newUser

            newUser
        } ?: throw RuntimeException("User id not found")
    }

    private fun userInfo(
        chat: Chat,
        user: User
    ): String = "$user on chat $chat"
}
