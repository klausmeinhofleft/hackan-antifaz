package be.rlab.antifaz.config

import be.rlab.antifaz.bot.model.CommandPermissions
import be.rlab.antifaz.bot.model.Permission
import be.rlab.antifaz.bot.model.Role
import be.rlab.antifaz.util.ObjectMapperFactory
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory

data class ApplicationConfig(
    val roles: List<Role>,
    val admins: List<Long>,
    val commandsPermissions: List<CommandPermissions>
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(ApplicationConfig::class.java)

        fun load(configFile: String): ApplicationConfig {
            logger.info("Loading application configuration from classpath: $configFile")
            val jsonConfig: String = Thread.currentThread().contextClassLoader.getResource(configFile).readText()

            return ObjectMapperFactory.snakeCaseMapper.readValue(jsonConfig)
        }
    }

    fun isAdmin(userId: Long): Boolean = admins.contains(userId)

    fun findRole(roleName: String): Role {
        return roles.find { role ->
            role.name == roleName
        } ?: throw RuntimeException("Role not found")
    }

    fun findCommandPermissions(commandName: String): List<Permission> {
        return commandsPermissions.find { commandPermissions ->
            commandPermissions.commandName == commandName
        }?.requiredPermissions ?: emptyList()
    }
}
