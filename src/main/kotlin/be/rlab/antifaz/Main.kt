package be.rlab.antifaz

import be.rlab.antifaz.bot.AccessControl
import be.rlab.antifaz.bot.Antifaz
import be.rlab.antifaz.command.email.EmailService
import be.rlab.antifaz.bot.Memory
import be.rlab.antifaz.command.calendar.CalendarAddEventCommand
import be.rlab.antifaz.command.calendar.CalendarViewCommand
import be.rlab.antifaz.command.email.AddEmailAccountCommand
import be.rlab.antifaz.command.email.ListEmailCommand
import be.rlab.antifaz.command.email.ReadEmailCommand
import be.rlab.antifaz.command.user.AddUserCommand
import be.rlab.antifaz.command.user.ListChatsCommand
import be.rlab.antifaz.config.ApplicationConfig
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import java.io.File

class Main {

    private val logger: Logger = LoggerFactory.getLogger(Main::class.java)

    fun start() {
        logger.info("Antifaz is waking up")

        val memoryDirEnv: String = System.getenv("memory_dir")
        val accessToken: String = System.getenv("access_token")

        logger.info("Restoring memory from $memoryDirEnv")
        logger.info("Using access token $accessToken")

        val memoryDir = File(memoryDirEnv)
        if (!memoryDir.exists()) {
            logger.info("Memory directory not found, creating")
            memoryDir.mkdirs()
        }

        val applicationConfig = ApplicationConfig.load("applicationConfig.json")
        val memory = Memory.wakeUp(memoryDir)
        val accessControl = AccessControl(applicationConfig, memory)
        val emailService = EmailService(memory)
        val antifaz = Antifaz(
            accessToken = accessToken,
            accessControl = accessControl,
            botListeners = listOf(emailService),
            commands = listOf(
                ListEmailCommand(emailService),
                ReadEmailCommand(emailService, accessControl),
                AddEmailAccountCommand(memory, accessControl),
                ListChatsCommand(accessControl),
                AddUserCommand(applicationConfig, accessControl),
                CalendarViewCommand(memory),
                CalendarAddEventCommand(memory)
            )
        )

        logger.info("Antifaz is entering Telegram")
        antifaz.start()
    }
}

fun main(args: Array<String>) {
    // Sets up jul-to-slf4j bridge. I have not idea
    // why the logging.properties strategy doesn't work.
    SLF4JBridgeHandler.removeHandlersForRootLogger()
    SLF4JBridgeHandler.install()

    Main().start()
}
