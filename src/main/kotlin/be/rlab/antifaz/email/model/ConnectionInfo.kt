package be.rlab.antifaz.email.model

data class ConnectionInfo(
    val host: String,
    val port: Int,
    val userName: String,
    val password: String,
    val connectionProperties: Map<String, String> = emptyMap()
) {
    override fun toString(): String = "$host/$userName"
}
