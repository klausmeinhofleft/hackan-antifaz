package be.rlab.antifaz.email.model

import javax.mail.Flags
import javax.mail.search.FlagTerm
import javax.mail.search.SearchTerm

enum class Flag(
    private val systemFlag: Flags.Flag,
    val term: SearchTerm
) {
    SEEN(Flags.Flag.SEEN, FlagTerm(Flags(Flags.Flag.SEEN), true)),
    UNSEEN(Flags.Flag.SEEN, FlagTerm(Flags(Flags.Flag.SEEN), false)),
    ANSWERED(Flags.Flag.ANSWERED, FlagTerm(Flags(Flags.Flag.ANSWERED), true)),
    DELETED(Flags.Flag.DELETED, FlagTerm(Flags(Flags.Flag.DELETED), true)),
    FLAGGED(Flags.Flag.FLAGGED, FlagTerm(Flags(Flags.Flag.FLAGGED), true)),
    DRAFT(Flags.Flag.DRAFT, FlagTerm(Flags(Flags.Flag.DRAFT), true)),
    UNKNOWN(Flags.Flag.USER, FlagTerm(Flags(Flags.Flag.USER), true));

    companion object {
        fun from(flags: Flags): List<Flag> {
            return flags.systemFlags.map { systemFlag ->
                values().find { flag ->
                    flag.systemFlag == systemFlag
                } ?: UNKNOWN
            }
        }
    }
}