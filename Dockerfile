FROM openjdk:8-jre
MAINTAINER seykron <seykron@rlab.be>

ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/antifaz/antifaz.jar", "-Daccess_token=${BOT_ACCESS_TOKEN}"]

ARG JAR_FILE
# Add the service itself
ADD target/$JAR_FILE /usr/share/antifaz/antifaz.jar
